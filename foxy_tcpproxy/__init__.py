#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) Smart Arcs Ltd, Enigma Bridge Ltd, registered in the United Kingdom.
# This file is owned exclusively by Radical Prime Limited.
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Dan Cvrcek <support@smartarchitects.co.uk>, August 2018

__copyright__ = "Smart Arcs Ltd, Enigma Bridge Ltd"
__email__ = "support@smartarchitects.co.uk"
__status__ = "Beta"
